import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DatePicker } from '@ionic-native/date-picker';
import moment from 'moment';
import { DatePickerModule } from 'datepicker-ionic2';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  todo :any;
  localDate: any;
  ionicDate: any;
  ionicDate1: any;
  ionicDate2: any;
  ionicDate3: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private datePicker: DatePicker,
    private formBuilder: FormBuilder) {
      this.localDate = new Date();

      this.todo = this.formBuilder.group({
      title: ['', Validators.required],
             date1: ['1985-02-22'],
             date2: ['']
      });

  }

   showDatePicker(input) {
	
	let selectedDate;
	if(input.value == null || input.value == '' ) { 
		selectedDate = new Date();
	} else {
		selectedDate = new Date(input.value);
	}
	 
	  
    this.datePicker.show({
	  date: selectedDate,
      mode: 'date',
      maxDate : new Date().getTime(),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT  
    }).then(
      date => input.value = moment(date).format('YYYY-MM-DD'),
      err => console.log('Error occurred while getting date: ', err)
    );
  }


  datepickerIonic(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => this.ionicDate = date,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  
  
datepickerIonic1(){
	this.datePicker.show({
	  date: new Date(),
	  mode: 'string',
	  androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
	}).then(
	  date => this.ionicDate1 = date,
	  err => console.log('Error occurred while getting date: ', err)
	);
}	

datepickerIonic2(){
	this.datePicker.show({
	  date: new Date(),
	  mode: 'date',
	  maxDate : new Date().getTime(),
	  androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
	}).then(
	  date => this.ionicDate2 = date,
	  err => console.log('Error occurred while getting date: ', err)
	);
}

datepickerIonic3(){
	this.datePicker.show({
	  date: new Date(),
	  mode: 'string',
	  maxDate : new Date(),
	  androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
	}).then(
	  date => this.ionicDate3 = date,
	  err => console.log('Error occurred while getting date: ', err)
	);
}

  setDate(event){
    console.log(event);
    this.localDate = event;
  }
}
